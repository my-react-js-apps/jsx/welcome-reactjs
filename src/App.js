import './App.css';

const gReact = {
  title: 'Welcome to React',
  image: 'https://reactjs.org/logo-og.png',
  benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
  studyingStudents: 150,
  totalStudents: 200
}

const rateStudyingStudents = () => gReact.studyingStudents / gReact.totalStudents * 100;

function App() {
  return (
    <div className="App">
      <h1>{gReact.title}</h1>
      <img src = {gReact.image} width={"500px"} />
      <p>Rate Studying Students: {rateStudyingStudents()}%</p>
      <p>{rateStudyingStudents() > 20 ? 'Rate Studying Students is hight!' : 'Rate Studying Students is low!'}</p>
      <ul>
        {
          gReact.benefits.map((value, index) => {
            return <li key={index}>{value}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
